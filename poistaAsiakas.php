<?php
require_once "pdo.php";
session_start();
if ( isset($_POST['poista']) && isset($_POST['asID'])) {
	echo "toimii";
	$sql = "DELETE FROM asiakkaat WHERE asID = :zip";
	$stmt = $conn->prepare($sql);
	$stmt->execute(array(':zip' => $_POST['asID']));
	$_SESSION['success'] = 'Record deleted';
}
$stmt = $conn->prepare("SELECT asID, asEtunimi, asSukunimi FROM asiakkaat where asID = :xyz");
$stmt->execute(array(":xyz" => $_GET['parametri']));
$row = $stmt->fetch(PDO::FETCH_ASSOC);

?>

<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>rekisteröinti</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
  </head>
  <body>
  <div class="container">
    <h1>Oletko varma että haluat poistaa käyttäjän <?= htmlentities($row['asEtunimi']) ?> <?= htmlentities($row['asSukunimi']) ?>?</h1>
	<form method="POST">
	<input type="hidden" name="asID" value="<?= $row['asID'] ?>">
	<button type="submit" class="btn btn-primary" name="poista">Lähetä</button>
	<a class="btn btn-primary" href="adminselaus.php" name="ei">En ole varma</a>
	</form>
</div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-OERcA2EqjJCMA+/3y+gxIOqMEjwtxJY7qPCqsdltbNJuaOe923+mo//f6V8Qbsw3" crossorigin="anonymous"></script>
  </body>
</html>