<!doctype html>
<html lang="fi">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>PHP avustus</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
  </head>
  <body>
    <h1>Adminointi</h1>
	<?php 
	require_once 'pdo.php'; //yhdistää
	try {
	  $stmt = $conn->query("SELECT * FROM asiakkaat");
	  $result = $stmt->FetchAll(PDO::FETCH_ASSOC);
	  $row = $stmt->fetch(PDO::FETCH_ASSOC);
	}
	catch(PDOexception $e) {
		echo "Ei toimi" . $e->getMessage();
	}

	echo '<div class="Container">
	<table class="table table-dark table-striped-columns table-hover table-bordered border-info">'
	."\n";
	echo '<thead><tr>
			<th scope="col">ID</th>
			<th scope="col">Sähköposti</th>
			<th scope="col">Etunimi</th>
			<th scope="col">Sukunimi</th>
			<th scope="col">Puhelin</th>
			<th scope="col">AloitusPVM</th>
			<th scope="col">Muuta</th>
			<th scope="col">Poista</th>
		  </tr>
		</thead><tbody>';
	foreach ($result as $row) {
		$valitettavaID = $row['asID'];
		echo "<tr><td>";
		echo($row['asID']);
		echo ("</td><td>");
		echo($row['asSposti']);
		echo ("</td><td>");
		echo($row['asEtunimi']);
		echo ("</td><td>");
		echo($row['asSukunimi']);
		echo ("</td><td>");
		echo($row['asPuhelin']);
		echo ("</td><td>");
		echo($row['asAloitusPVM']);
		echo ("</td><td>");
		echo "<a href='muutaAsiakas.php? parametri=$valitettavaID'>Muuta</a>";
		echo ("</td><td>");
		echo "<a href='poistaAsiakas.php? parametri=$valitettavaID'>Poista</a>";
		echo ("</td></tr>");
	}
	echo "</tbody></table></div>\n";
	?> <!-- PHP loppuu tähän!!! -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-OERcA2EqjJCMA+/3y+gxIOqMEjwtxJY7qPCqsdltbNJuaOe923+mo//f6V8Qbsw3" crossorigin="anonymous"></script>
  </body>
</html>