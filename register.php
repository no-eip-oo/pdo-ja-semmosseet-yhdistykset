<?php
require_once "pdo.php";
session_start();
if ( isset($_POST['kayttaja']) && isset($_POST['salasana']) && isset($_POST['sPosti'])) {
    $sql = "INSERT INTO kayttajatiedot (kayttaja, salasana, sPosti)
        VALUES (:kayttaja, :salasana, :sPosti)";
    $stmt = $conn->prepare($sql);
    $stmt->execute(array(
        ':sPosti' => $_POST['sPosti'],
        ':kayttaja' => $_POST['kayttaja'],
        ':salasana' => $_POST['salasana']));
    $_SESSION['success'] = 'Record Added';
    header( 'Location: login.php' );
    return;
}
?>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
<link rel="stylesheet" href="../CSS/formi.css">
<body>
    <div class="container">
        <form method="post"> 
            <div class="mb-3">
                <label for="sPosti" class="form-label">Sähköposti</label>
                <input name="sPosti" type="email" class="form-control" id="sPosti" required>
            </div>
            <div class="mb-3">
                <label for="kayttaja" class="form-label">Käyttäjänimi</label>
                <input name="kayttaja" type="text" class="form-control" id="kayttaja" required>
            </div>
            <div class="mb-3">
                <label for="Salasana" class="form-label">Salasana</label>
                <input name="salasana" type="password" class="form-control" id="Salasana" required>
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-OERcA2EqjJCMA+/3y+gxIOqMEjwtxJY7qPCqsdltbNJuaOe923+mo//f6V8Qbsw3" crossorigin="anonymous"></script>
</body>
