$(document).ready(function() { //aloittaa funktion suorittamisen sitä kutsuessa
	$("#formi").submit(function(e) { //Funktion sisältö suorittuu kun formin sisällä olevaa submit nappia painetaan
		e.preventDefault();
		alert("nappi");
		var form = $(this);
		$.ajax({ //käytetään ajaxia tietokantaan yhdistämiseen
			type: form.attr('method'),
			cache: false,
			url: form.attr('action'),
			data: form.serialize(),
			dataType: "text",
			success: function(response) {
				console.log("palauttaa: " + response);
			}
		});
	});
});
