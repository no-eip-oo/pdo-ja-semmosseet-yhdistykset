<?php
require_once "pdo.php";
session_start();
if ( isset($_POST['asSposti']) && isset($_POST['asEtunimi']) && isset($_POST['asSukunimi']) && isset($_POST['user_id']) && isset($_POST['asPuhelin'])) {
	echo "toimii";
	$sql = "UPDATE asiakkaat SET 
			asEtunimi = :name,
			asSposti = :email, 
			asSukunimi = :snimi,
			asPuhelin = :puhelin
			WHERE asID = :user_id";
	$stmt = $conn->prepare($sql);
	$stmt->execute(array(
		':name' => $_POST['asEtunimi'],
		':email' => $_POST['asSposti'],
		':snimi' => $_POST['asSukunimi'],
		':user_id' => $_POST['user_id'],
		':puhelin' => $_POST['asPuhelin']));
}
else echo"ei toimi";
echo $_GET['parametri'];
$stmt = $conn->prepare("SELECT * FROM asiakkaat where asID = :xyz");
$stmt->execute(array(":xyz" => $_GET['parametri']));
$row = $stmt->fetch(PDO::FETCH_ASSOC);

$en = htmlentities($row['asEtunimi']);
$sn = htmlentities($row['asSukunimi']);
$e = htmlentities($row['asSposti']);
$ph = htmlentities($row['asPuhelin']);
$pv = htmlentities($row['asAloitusPVM']);
$ak = htmlentities($row['asKieli']);
$asID = $row['asID'];
?>

<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Muuta</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
  </head>
  <body>
  <div class="container">
    <h1>Muuta asiakkaan tietoja</h1>
	<form method="POST">
	<input type="hidden" name="user_id" value="<?= $asID ?>">
	  <div class="mb-3">
		<label for="exampleInputEmail1" class="form-label">Sähköposti</label>
		<input type="email" class="form-control" id="exampleInputEmail1" name="asSposti" value="<?= $e ?>" required>
	  </div>
	<div class="row">
	  <div class="col">
		<label for="Etunimi" class="form-label">Etunimi</label>
		<input type="text" class="form-control" id="Etunimi" aria-label="First name" name="asEtunimi" value="<?= $en ?>" required>
	  </div>
	  <div class="col">
		<label for="Sukunimi" class="form-label">Sukunimi</label>
		<input type="text" class="form-control" id="Sukunimi" aria-label="Last name" name="asSukunimi" value="<?= $sn ?>" required>
	  </div>
	</div>
	<div class="mb-3">
		<label for="puhelinnumero" class="form-label">Puhelinnumero</label>
		<input type="text" class="form-control" id="puhelinnumero" name="asPuhelin" value="<?= $ph ?>" required>
	</div>
		<div class="mb-3">
		<label for="paivamaara" class="form-label">Aloitus päivämäärä</label>
		<input type="text" class="form-control" id="paivamaara" name="paivaM" value="<?= $pv ?>" required>
	</div>
	<div class="mb-3">
	<label for="kieli" class="form-label">Asiointikieli</label>
	<select class="form-select" required aria-label="Default select example" id="kieli" name="asKieli">
	  <option value="<?= $ak ?>"><?= $ak ?></option>
	  <option value="suomi">suomi</option>
	  <option value="ruotsi">ruotsi</option>
	  <option value="englanti">englanti</option>
	</select>
	</div>
	  <button type="submit" class="btn btn-primary">Päivitä</button>
	  <a class="btn btn-primary" href="adminselaus.php" name="ei">Takaisin</a>
	</form>
</div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-OERcA2EqjJCMA+/3y+gxIOqMEjwtxJY7qPCqsdltbNJuaOe923+mo//f6V8Qbsw3" crossorigin="anonymous"></script>
  </body>
</html>