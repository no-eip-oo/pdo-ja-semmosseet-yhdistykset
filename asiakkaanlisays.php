<?php
/*Tämä php koodi tulee siirtämään halutut tiedot tietokantaan*/
/*Tässä kohdassa yhdistetään pdo tiedoston avulla tietokantaan*/
require_once "PDO.php";

function test_input($data) { //Testataan syötetyt tiedot SQL-injectioiden varalta
	$data = trim($data);
	$data = stripslashes($data);
	$data = htmlspecialchars($data);
	return $data;
}
$sposti = $sala = $etunimi = $sukunimi = $puhelin = $kieli = ""; //muuttujat asetetaan tyhjiksi
$pvm = date('y-m-d'); //Päivämäärä asetetaan tässä

if ($_SERVER["REQUEST_METHOD"] == "POST") { //Annetaan muuttujille arvoksi POSTin sisällä oleva data, ja pidetään huolta että ne ovat turvallisia
	$sposti = test_input($_POST["sposti"]);
	$salasana = test_input($_POST["salasana"]);
	$etunimi = test_input($_POST["etunimi"]);
	$sukunimi = test_input($_POST["sukunimi"]);
	$puhelin = test_input($_POST["puhelin"]);
	$kieli = test_input($_POST["kieli"]);
	//laitetaan salasanakin turvaan
	$salasana = password_hash($sala, PASSWORD_DEFAULT);
	//Ja stten kirjoitetaan SQL koodi joka iskee tiedot pöytään
	$sql = "INSERT INTO rekisteri (Sposti, Etunimi, Salasana, Sukunimi, puhelin, asiointikieli, aloitusPVM)
				VALUES (:sposti, :etunimi, :salasana, :sukunimi, :puhelin, :asiointikieli, :pvm )";
	$stmt = $conn->prepare($sql); //stmt nimisen funktion sisään laitetaan PDO tiedoston sisällä tapahtuva yhdistäminen, ja se laitetaan valmiiksi käyttöön

	$stmt->execute(array( //annetaan eri nimet funktioille jotka sisältävät formiin syötetyt tiedot
		':sposti' => $sposti,
		':salasana' => $salasana,
		':etunimi' => $etunimi,
		':sukunimi' => $sukunimi,
		':puhelin' => $puhelin,
		':asiointikieli' => $kieli,
		':pvm' => $pvm));
	echo "Tiedot lisätty";
}
?>
